from selenium import webdriver
from endpoints.ml.Scraper import Scraper
from endpoints.ml.ml import *
from pymongo import MongoClient
from multiprocessing.dummy import Pool, cpu_count

# Set db connection
client = MongoClient('mongodb://127.0.0.1:27017/')
# Set db
db = client['promarketer2']
# Get vendor collection
vendors = db.vendors_stage23

def scrape_keys_selenium(v):

	## Set vendor url ##
	url = v['domain']['url']
	# vendor_domain = "linkscreative.co.uk"

	# print "Getting keys for", url

	try:

		# create a new Firefox session
		driver = webdriver.Chrome()
		driver.implicitly_wait(10)
		driver.get(url)

		html = driver.page_source

		driver.close()

		scraper = Scraper()
		text = scraper.text_from_html(html)

		if text != None:

			try:
				
				raw_keywords = extractKeywordsText(text)
				keywords = json.loads(raw_keywords)

				filtered = {k: v for k, v in keywords.items() if v > 0.15}

				try:

					vendor_id = v['_id']
					vendors.update(
						{'_id': vendor_id},
						{'$set':
							{
								'domain.keywords': filtered
							}
						}
					)

					print "Saved keys:", filtered

				except:

					print "Failed to save keywords"

			except:

				print "Failed to get keywords (scrape_keys)"

		else:

			print "Domain returned None"

	except:
	
		print "Failed to scrape domain"



if __name__ == '__main__':

	# scope = list(vendors.find({ "$or": [ {'company_domain':'140proof.com'}, {'company_domain':'aarki.com'}, {'company_domain':'360dialog.com'} ] }))
	# scope = list(vendors.find({ "$or": [ {'company_domain':'aarki.com'} ] }))
	# scope = list(vendors.find({ "$or": [ {'domain.domain':'venturescanner.com'} ] }))
	# scope = list(vendors.find({ 'domain.status': 204}))
	# scope = list(vendors.find({ 'domain.domain': { '$exists': True }}))
	scope = list(vendors.find({ "$and": [ {'domain.url': { '$exists': True }}, {'domain.keywords': { '$exists': False }}, {'domain.ok':True} ] }))

	ITERATION_COUNT = cpu_count()-1

	print "CPUS:", ITERATION_COUNT

	pool = Pool(ITERATION_COUNT) 
	pool.map(scrape_keys_selenium, scope)
	pool.close() 
	pool.join()