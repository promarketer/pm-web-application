from mongoengine import *

def connect_to_mongo(db, db_host):
    print('Connecting to MongoDB')
    connect(db, host=db_host)

import json
from bson import ObjectId

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

class Brand(Document):
    company_name = StringField(required=True, max_length=300)
    company_description = StringField(required=False, max_length=50000)
    company_domain = StringField(required=True, max_length=300)
    company_url = StringField(required=True)
    keywords = ListField(StringField(max_length=50), max_length=50000)
    name = StringField(required=True, max_length=300)
    email = StringField(required=True, max_length=50)
    password = StringField(required=True, max_length=50)
    avatar = BinaryField(required=False)

class Vendor(Document):
    company_name = StringField(required=True, max_length=50000)
    company_description = StringField(required=False, max_length=50000)
    company_domain = StringField(required=True, max_length=50000)
    company_url = StringField(required=True)
    keywords = ListField(StringField(max_length=500), max_length=500000)
    name = StringField(required=True, max_length=50000)
    email = StringField(required=True, max_length=50000)
    password = StringField(required=True, max_length=50000)
    avatar = BinaryField(required=False)
    image = BinaryField(required=False)
    logo = StringField(required=False, max_length=50000)

class VendorNew(Document):
    parent_category = StringField(required=False, max_length=50000)
    child_category = StringField(required=False, max_length=50000)
    status = StringField(required=False, max_length=50000)
    name = StringField(required=False, max_length=50000)
    domain =  StringField(required=False, max_length=50000)
    logo = StringField(required=False, max_length=50000)

class Solution(Document):
    vendorAccountId = StringField(required=True, max_length=50000)
    name = StringField(required=True, max_length=50000)
    market = StringField(required=False, max_length=50000)
    url = StringField(required=True, max_length=50000)
    description = StringField(required=True, max_length=50000)
    categories = ListField(StringField(max_length=50), required=True, max_length=50000)
    brands = StringField(required=False, max_length=50000)
    keywords = ListField(StringField(max_length=500), max_length=500000)

class Brief(Document):
    brandAccountId = StringField(required=True, max_length=50000)
    description = StringField(required=True, max_length=50000)
    keywords = ListField(StringField(max_length=50), required=True, max_length=50000)
    categories = ListField(StringField(max_length=50), required=True, max_length=50000)
    urgency = StringField(required=True, max_length=50000)
    budget = StringField(required=True, max_length=50000)

class Match(Document):
    matchType = StringField(required=True, max_length=50000)
    brandId = StringField(required=True, max_length=50000)
    briefId = StringField(required=True, max_length=50000)
    solutionId = StringField(required=True, max_length=50000)
    score = StringField(required=True, max_length=50000)
