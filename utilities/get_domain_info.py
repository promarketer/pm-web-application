from pymongo import MongoClient
from server import app
from urlparse import urlparse
import requests
from utilities.clean_domain import cleanDomain
from utilities.get_domain_keywords import getDomainKeywords

# Access db
client = MongoClient(app.config['DB_HOST'])
db = client[app.config['DB_NAME']]

# Set request params
headers = {'User-Agent': 'Mozilla/5.0'}
timeout = 4.0

def getDomainInfo(domain):

	app.logger.info('Getting info for %s', domain)

	domain_info = {}

	## Build URL
	url = "http://" + domain

	try:
		r = requests.get(url, headers=headers, timeout=timeout)

		# Assign base domain properties
		try:
			app.logger.info('Attempting to assign domain attributes')
			domain_info['reference'] = domain
			domain_info['url'] = r.url
			domain_info['domain'] = cleanDomain(domain_info['url'])
			domain_info['status_code'] = r.status_code

			# Get keywords
			keywords = getDomainKeywords(domain_info['url'])
			if keywords != None:
				domain_info['keywords'] = keywords
			
			# Derive domain OK status
			if r.status_code == requests.codes.ok:
				domain_info['ok'] = True
			else:
				domain_info['ok'] = False

			# Derive differs_from_reference status
			if domain_info['domain'] != domain_info['reference']:
				domain_info['differs_from_reference'] = True
			else:
				domain_info['differs_from_reference'] = False

		except Exception as e:
			app.logger.error('Error assigning domain attributes.')
			print e
			raise

		# Assign redirects
		app.logger.info('Checking whether redirects exist')
		if r.history:
			domain_info['redirects'] = []
			for idx, val in enumerate(r.history):
				step = {}
				try:
					app.logger.info('Assigning redirect %s', idx)
					step['index'] = idx
					step['url'] = val.url
					step['status'] = val.status_code
					domain_info['redirects'].append(step)
				except Exception as e:
					app.logger.error('Error assigning redirect step.')
					print e
					raise

		return domain_info

	except Exception as e:
		app.logger.error('Error assigning status info to domain info.')
		print e
		raise