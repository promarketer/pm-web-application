from server import app
import clearbit

clearbit.key = 'sk_d53647ca6403dfa44b942e67e012abad'

def getClearbitData(domain):
	try:
		app.logger.info('Searching clearbit for %s', domain)
		data = clearbit.Enrichment.find(domain=domain, stream=True)
		return data
	except Exception as e:
		app.logger.error('Error searching  domain attributes.')
		print e
		raise