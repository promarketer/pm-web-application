import os
from server import app
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from endpoints.ml.Scraper import Scraper
from endpoints.ml.ml import *

def getDomainKeywords(domain):

	try:
		app.logger.info('Attempting to get keywords for %s', domain)

		if not domain.startswith("http://") and not domain.startswith("https://") and not domain.startswith("//"):
		    url = "http://" + domain
		else:
			url = domain

		chrome_bin = os.environ.get('GOOGLE_CHROME_SHIM', None)
		print "Chrome bin:", chrome_bin
		if chrome_bin != None:
			opts = ChromeOptions()
			opts.binary_location = chrome_bin
			browser = webdriver.Chrome(executable_path="chromedriver", chrome_options=opts)
		else:
			browser = webdriver.Chrome()
		browser.implicitly_wait(10)
		browser.get(url)

		html = browser.page_source

		browser.close()

		# Get text from HTML
		scraper = Scraper()
		text = scraper.text_from_html(html)
		if text != None:
			try:
				raw_keywords = extractKeywordsText(text)
				keywords = json.loads(raw_keywords)
				filtered = {k: v for k, v in keywords.items() if v > 0.15}
				return filtered
			except Exception as e:
				app.logger.info('Failed to get keywords from text')
				print e
				return None
		else:
			app.logger.info('Failed to get text from HTML')
			return None
	except Exception as e:
		app.logger.info('Failed to scrape HTML')
		print e
		return None