from server import app
from urlparse import urlparse

def cleanDomain(domain):

	app.logger.info('Cleaning %s', domain)

	if not domain.startswith("http://") and not domain.startswith("https://") and not domain.startswith("//"):
	    url = "http://" + domain
	else:
		url = domain

	try:
		parsed_url = urlparse(url)
		netloc = parsed_url.netloc
		if netloc.startswith("www."):
		    domain = netloc[4:]
		else:
			domain = netloc
	except Exception as e:
	    app.logger.error('Error parsing url.')
	    print e
	    raise

	return domain