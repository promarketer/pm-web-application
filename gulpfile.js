var gulp = require('gulp');
var less = require('gulp-less');
var rename = require("gulp-rename");

// Compile LESS files from /less into /css
gulp.task('less', function() {
    return gulp.src('static/less/main.less')
        .pipe(less())
        .pipe(gulp.dest('static/dist/css'))
});

// Minify compiled CSS
gulp.task('minify-css', ['less'], function() {
    return gulp.src('static/dist/css/main.css')
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('static/dist/css'))
});

// Dev task
gulp.task('dev', ['less', 'minify-css'], function() {
    gulp.watch('static/less/main.less', ['less']);
    gulp.watch('static/dist/css/main.css', ['minify-css']);
});

gulp.task('default', ['less', 'minify-css']);
