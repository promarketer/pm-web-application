from pymongo import MongoClient
import urllib3
import requests
import json
from multiprocessing.dummy import Pool as ThreadPool

# Set db connection
client = MongoClient('mongodb://127.0.0.1:27017/')
# Set db
db = client['promarketer2']
# Get vendor collection
vendors = db.vendors_stage23

headers = {'User-Agent': 'Mozilla/5.0'}
timeout = 4.0

def getDomainInfo(v):
	
	## Set vendor domain ##
	vendor_id = v['_id']

	## Set vendor domain ##
	url = "https://" + v['company_domain']

	print "Analysing", url

	try:
		r = requests.get(url, headers=headers, timeout=timeout)
		# Update base domain properties
		try:
			
			if r.status_code == requests.codes.ok:
				ok_status = True
			else:
				ok_status = False

			# Get domain from r.url
			parsed_url = urlparse(r.url)
			domain = parsed_url.netloc
			if domain.startswith("www."):
				domain_clean = domain[4:]
			else:
				domain_clean = domain

			vendors.update(
				{
					'_id': vendor_id
				},
				{
					'$set': {
						'domain.reference': v['company_domain'],
						'domain.url': r.url,
						'domain.domain': domain_clean,
						'domain.status': r.status_code,
						'domain.ok': ok_status,
						'domain.redirects': [],
					}
				}
			)
			print "Successfully set base properties"
			try:
				for idx, val in enumerate(r.history):
					vendors.update(
						{
							'_id': vendor_id
						},
						{
							'$push': {
								"domain.redirects": {
									'index': idx,
									'url': val.url,
									'status': val.status_code
								},
							}
						}
					)

				print "Successfully set redirects"
			except:
				print "Failed to set redirects"
		except:
			print "Failed to update base properties"
	except requests.exceptions.RequestException as e:
		print e
		print "Unknown error occured."

if __name__ == '__main__':

	# scope = list(vendors.find({ "$or": [ {'company_domain':'peadler.com'} ] }))
	# scope = list(vendors.find({ 'domain.status': 204}))
	# scope = list(vendors.find({}))
	scope = list(vendors.find({ '$and': [{'domain': { '$exists': False }}] }))

	pool = ThreadPool(50) 
	pool.map(getDomainInfo, scope)
	pool.close() 
	pool.join()