#!/usr/bin/python
import os
import sys
from flask import Flask, render_template, request
import json
import logging
from flask_cors import CORS
import datetime

app = Flask(__name__, static_folder='static', template_folder='templates')

app.config.from_object(os.environ['APP_SETTINGS'])

# set app config to env variable APP_SETTINGS
print(os.environ['APP_SETTINGS'])

from storage.data_models import connect_to_mongo, Vendor, Brand
from endpoints.backend import *
from endpoints.ml_backend import *

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

connect_to_mongo(app.config['DB_NAME'], app.config['DB_HOST'])

def start_server():
    global app
    CORS(app)

start_server()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5005, debug=True, threaded=True)
