from gensim.summarization import summarize , keywords
from rake_nltk import Rake,Metric
import json

def summarizeText(text):
    try:
        summary = summarize(text)
    except:
        print "Something went wrong"
        summary = text
    return summary


def extractByRake(text):
	r = Rake(ranking_metric=Metric.DEGREE_TO_FREQUENCY_RATIO)
	try:
	    r.extract_keywords_from_text(text)
	    phrases = r.get_ranked_phrases()
	    print phrases
	    summary = ' '.join(phrases[:10])

	except:
	    print "Something went wrong"
	    summary = ""
	return summary

def extractByGensim(text):
	keyWords = keywords(text , scores = True, lemmatize = True, words = 10 )
	result = {}
	for k in keyWords:
		result[k[0]] = k[1]
	return json.dumps(result)

def extractKeywordsText(text):
	return extractByGensim(text)
    
