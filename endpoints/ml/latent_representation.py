import dynet_config

dynet_config.set(mem=1, random_seed=9, autobatch=True)
dynet_config.set_gpu()
import dynet as dy


class KeywordEncoder:
    def __init__(self, latent_size=2):
        self.model = dy.Model()
        self.trainer = dy.AdamTrainer(self.model)
        self.latent_size = latent_size
        self.word2int = {}

    def load(self, filename):
        with open(filename + ".encodings", "r") as f:
            for line in f.readlines():
                parts=line.split("\t")
                index=int(parts[1])
                word=parts[0]
                self.word2int[word]=index
        self.w=self.model.add_parameters((self.latent_size, len(self.word2int)))
        self.model.populate(filename+".network")

    def save(self, filename):
        print ("\tStoring " + filename)
        with open(filename + ".encodings", "w") as f:
            for word in self.word2int:
                f.write(word + "\t" + str(self.word2int[word]) + "\n")
            f.close()

        self.model.save(filename + ".network")

    def _encode(self, word_list):
        input = [0.0] * len(self.word2int)
        for word in word_list:
            if word in self.word2int:
                input[self.word2int[word]] = 1.0
            if word.lower() in self.word2int:
                input[self.word2int[word.lower()]] = 1.0
            if word.title() in self.word2int:
                input[self.word2int[word.title()]] = 1.0
        return input

    def learn(self, word_list):
        encoded = self._encode(word_list)
        encoder_vec = dy.inputVector(encoded)

        proj = self.w.expr() * encoder_vec

        reconstruct = dy.transpose(self.w.expr()) * proj
        loss = dy.squared_distance(reconstruct, encoder_vec)

        return loss

    def project(self, word_list):
        encoded = self._encode(word_list)
        encoder_vec = dy.inputVector(encoded)

        proj = dy.logistic(self.w.expr() * encoder_vec)
        return proj

    def build_from_dataset(self, filename, store_location):
        word2count = {}
        with open(filename, "r") as f:
            for line in f.readlines():
                parts = line.replace("\n", "").replace("\r", "").split(" ")
                for part in parts:
                    if part not in word2count:
                        word2count[part] = 1
                    else:
                        word2count[part] = word2count[part] + 1

        for word in word2count:
            if word2count[word] >= 2:
                self.word2int[word] = len(self.word2int)

        print ("Found " + str(len(self.word2int)) + " keywords after prunning")

        MAX_ITT = 10
        num_itt_no_improve = MAX_ITT
        best_loss = None
        epoch = 0
        self.w = self.model.add_parameters((self.latent_size, len(self.word2int)))
        losses = []
        while num_itt_no_improve > 0:
            num_itt_no_improve -= 1
            epoch += 1
            total_loss_value = 0
            print ("Starting epoch " + str(epoch))
            dy.renew_cg()
            cnt = 0
            with open(filename, "r") as f:
                for line in f.readlines():
                    cnt += 1
                    parts = line.replace("\n", "").replace("\r", "").split(" ")
                    loss = self.learn(parts)
                    losses.append(loss)
                    if len(losses) == 100:
                        total_loss = dy.esum(losses)
                        total_loss_value += total_loss.value()
                        total_loss.backward()
                        self.trainer.update()
                        losses = []
                        dy.renew_cg()

                if len(losses) != 0:
                    total_loss = dy.esum(losses)
                    total_loss_value += total_loss.value()
                    total_loss.backward()
                    self.trainer.update()
                    losses = []
                    dy.renew_cg()
            avg_loss = total_loss_value / cnt
            if best_loss is None:
                best_loss = avg_loss
                self.save(store_location)
            if best_loss > avg_loss:
                best_loss = avg_loss
                num_itt_no_improve = MAX_ITT
                self.save(store_location)
            print ("\tavg_loss=" + str(avg_loss))

            # rendering
            import numpy as np
            import scipy.misc as smp

            all_data = np.zeros((cnt, self.latent_size))

            # Create a 1024x1024x3 array of 8 bit unsigned integers
            data = np.zeros((1024, 1024, 3), dtype=np.uint8)
            cnt = 0

            with open(filename, "r") as f:
                for line in f.readlines():
                    parts = line.replace("\n", "").replace("\r", "").split(" ")
                    coords = self.project(parts).npvalue()
                    all_data[cnt] = coords
                    dy.renew_cg()
                    cnt += 1

            var = np.var(all_data, axis=0)
            print var
            import heapq
            top_two = heapq.nlargest(2, range(len(var)), var.take)
            print top_two

            with open(filename, "r") as f:
                for line in f.readlines():
                    parts = line.replace("\n", "").replace("\r", "").split(" ")
                    coords = self.project(parts).npvalue()
                    x = int(coords[top_two[0]] * 1024)
                    y = int(coords[top_two[1]] * 1024)
                    data[x, y] = [255, 0, 0]
                    dy.renew_cg()
                    cnt += 1

            img = smp.toimage(data)  # Create a PIL image
            img.save(store_location + ".png")

    def get_distance(self, source_list, destination_list):
        dy.renew_cg()

        proj1 = self.project(source_list)
        proj2 = self.project(destination_list)
        return dy.squared_distance(proj1, proj2).value()


def match_test(kwe, brand_file, vendor_file):
    brands = {}
    vendors = {}
    with open(brand_file, "r") as f:
        for line in f.readlines():
            parts = line.replace("\n", "").replace("\r", "").split("\t")
            keywords = parts[1].split(' ')
            brand = parts[0]
            brands[brand] = keywords

    with open(vendor_file, "r") as f:
        for line in f.readlines():
            parts = line.replace("\n", "").replace("\r", "").split("\t")
            if len(parts) == 2:
                keywords = parts[1].split(' ')
                vendor = parts[0]
                vendors[vendor] = keywords

    total_time = 0
    for brand in brands:
        brand_keywords = brands[brand]
        vendor_list = []
        score_list = []
        for vendor in vendors:
            vendor_keywords = vendors[vendor]
            import time
            start=time.time()
            score = kwe.get_distance(vendor_keywords, brand_keywords)
            stop=time.time()
            total_time+=stop-start
            vendor_list.append(vendor)
            score_list.append(score)

        for i in xrange(len(vendor_list) - 1):
            for j in xrange(i + 1, len(vendor_list)):
                if score_list[j] < score_list[i]:
                    aux = score_list[j]
                    score_list[j] = score_list[i]
                    score_list[i] = aux
                    aux = vendor_list[i]
                    vendor_list[i] = vendor_list[j]
                    vendor_list[j] = aux
        sys.stdout.write("BRAND: " + brand + " matches")
        for i in xrange(min(5, len(vendor_list))):
            sys.stdout.write(" " + vendor_list[i] + " (" + str(score_list[i]) + ")")
        sys.stdout.write("\n")
        sys.stdout.flush()
    print "AVG distance computation time is "+str(total_time/(len(brands)*len(vendors)))

    return 0


if __name__ == '__main__':
    import sys

    latent_size = 20

    if len(sys.argv) < 2:
        print "Usage:"
        print "\t--train <keyword file> <model store base>"
        print "\t--test <model store base> <brand file> <vendor file>"

    if sys.argv[1] == '--train':
        kwe = KeywordEncoder(latent_size=latent_size)
        print ("Training Keyword latent space representation")
        kwe.build_from_dataset(sys.argv[2], sys.argv[3])

    if sys.argv[1] == '--test':
        kwe = KeywordEncoder(latent_size=latent_size)
        kwe.load(sys.argv[2])
        match_test(kwe, sys.argv[3], sys.argv[4])
