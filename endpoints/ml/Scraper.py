from bs4 import BeautifulSoup
from bs4.element import Comment
from ..backend import app
from ml import *
import requests
import urllib3
import json


class Scraper:
	maxLength = None
	def __init__(self,maxLength = None):
		if maxLength != None:
			self.maxLength = maxLength

	def text_from_html(self,body):
		try:
			soup = BeautifulSoup(body, 'html.parser')
			try:
				texts = soup.findAll(text=True)
				try:
					visible_texts = filter(self.tag_visible, texts)
					try:
						visible_texts_final = u" ".join(t.strip() for t in visible_texts)
						return visible_texts_final
					except:
						print "Could join visible texts"
				except:
					print "Could not get visible texts"
			except:
				print "Could not find all text"
		except:
			print "Could not parse HTML"

	def getKeywords(self, url):
		
		headers = {'User-Agent': 'Mozilla/5.0'}
		timeout = 2.0
		
		if not (url.startswith("http://") or url.startswith("https://")):

			url = "http://" + url

			print url

		try:

			r = requests.get(url, headers=headers, timeout=timeout)
			print "url:", r.url
			# Removed printing as causes error in Heroku
			# print "Text:", r.text
			print "Headers:", r.headers
			print "Status:", r.status_code
			print "History:", r.history

			try:

				text = self.text_from_html(r.text)

				if text != None:

					print "Seemed to get text from HTML but cannot print the result due to unknown error (see https://stackoverflow.com/questions/35096082/unicode-error-on-production-due-to-a-print-why)"

					try:
						
						keywords = extractKeywordsText(text)

						print "Keywords:", keywords

						filtered = {}

						print filtered

						json_keywords = json.loads(keywords)
						
						for k, v in json_keywords.iteritems():
							
							filtered[k] = v

						return json.dumps(filtered)

					except:

						print "Failed to get keywords (Scraper)"

				else:

					print "Domain returned None"

			except:

				print "Failed to get text from html"

		except:

			print "Connection failed"

	def tag_visible(self, element):
	    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
	        return False
	    if isinstance(element, Comment):
	        return False
	    return True

	def scrape(self, url):
		print "Scraping URL"
		headers = {'User-Agent': 'Mozilla/5.0'}
		timeout = 2.0

		if (not url.startswith("http://") or not url.startswith("http://")):
			url = "http://" + url
		try:
			r = requests.get(url, headers=headers, timeout=timeout)

			print "Text:", r.text
			try:
				text = self.text_from_html(r.text)
				print "Text from HTML:", text
				return text
			except:
				print "Failed to get text from html"
		except:
			print "Failed to get html"
	
	def extractLogo(self, url):
		soup = BeautifulSoup(urllib2.urlopen(url).read(), 'html.parser')
		urls = ''
		print '=================== \n'
		results = soup.find_all(id='logo')
		print str(results) + '\n=============='
		for x in results:
			try:
				if x.name == 'img':
					urls += x['src']
			except:pass
		return urls

	def getLogo(self, company_name, company_domain):
		apiRoot = "https://autocomplete.clearbit.com/v1/companies/suggest?query="
		url = apiRoot + company_name
		app.logger.info('#### Getting logo from %s ####', url)
		try:
			response = requests.get(url)
			if response.status_code == 200:
				results = json.loads(response.content)
				if len(results) > 0:
					for r in results:
						app.logger.info('Attempting to match: %s with %s', r["domain"].lower(), company_domain.lower())
						if r["domain"].lower() == company_domain.lower():
							app.logger.info('Domain matched!')
							logo = r["logo"]
							break
						else:
							app.logger.info('Domains not matched')
							logo = "Domains not matched"
					return logo
				else:
					return "No results match search"
			else:
				return "Not 200"
		except:
			return "Failed"

	def extractTwitterImage(self, company):
		app.logger.info('#### Extracting twitter image for %s ####', company)
		
		# set variables used later in the process
		crunchbase_base_url = 'https://www.crunchbase.com'
		crunchbase_query = '/textsearch?q=' + company
		crunchbase_query_url = crunchbase_base_url + crunchbase_query
		crunchbase_hdr = {'User-Agent': 'Mozilla/5.0'}

		# ==============================================================
		#  Step 1: Find company on Crunchbase and get url for profile
		# ==============================================================

		# Bind search results page to soup variable
		app.logger.info('Binding search results page to soup variable')
		req = urllib2.Request(crunchbase_query_url,headers=crunchbase_hdr)
		page = urllib2.urlopen(req)
		soup = BeautifulSoup(page, 'html.parser')

		# Get id cell for first result in list
		app.logger.info('Get id cell for first result in list')
		column_id_identifiers = soup.results.find_all('grid-cell', attrs={'class': 'column-id-identifier'}, limit=1)
		column_id_identifier = column_id_identifiers[0]
		
		# Get href for first link within that cell 
		app.logger.info('Get href for first link within that cell')
		result_links = column_id_identifier.find_all('a', attrs={'class': 'cb-link'}, limit=1)
		result_link = result_links[0]
		result_link_href = result_link['href']
		
		# Build url for company profile
		app.logger.info('Build url for company profile')
		result_url = crunchbase_base_url + result_link_href

		# ==============================================================
		#  Step 2: Load company profile and find Twitter profile url
		# ==============================================================

		# Bind company profile page to soup1 variable
		app.logger.info('Bind company profile page to soup1 variable')
		req1 = urllib2.Request(result_url,headers=crunchbase_hdr)
		page1 = urllib2.urlopen(req1)
		soup1 = BeautifulSoup(page1, 'html.parser')

		# Get url for Twitter profile view twitter link
		app.logger.info('Get url for Twitter profile view twitter link')
		twitter_links = soup1.find_all('a', attrs={'title': 'View on Twitter'}, limit=1)
		twitter_link = twitter_links[0]
		twitter_url = twitter_link['href']

		# ==============================================================
		#  Step 3: Get twitter background image on profile page
		# ==============================================================

		# Bind twitter profile page to soup2 variable
		app.logger.info('Bind twitter profile page to soup2 variable')
		req2 = urllib2.Request(twitter_url,headers=crunchbase_hdr)
		page2 = urllib2.urlopen(req2)
		soup2 = BeautifulSoup(page2, 'html.parser')

		# Get url for Twitter profile background image
		app.logger.info('Get url for Twitter profile background image')
		header_bgs = soup2.find_all('div', attrs={'class': 'ProfileCanopy-headerBg'}, limit=1)
		header_bg = header_bgs[0]
		header_bg_src = header_bg.img['src']

		# ==============================================================
		#  Step 4: Return url for twitter background image
		# ==============================================================
		app.logger.info('Retruning src of first twitter bg div img element')
		return header_bg_src