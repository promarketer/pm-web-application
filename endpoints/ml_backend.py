from pymongo import MongoClient
from backend import app
from ml.Scraper import *
from ml.ml import *
from flask import Flask, render_template, request
from utilities.get_domain_keywords import getDomainKeywords
from algoliasearch import algoliasearch
from server import app
# from storage.data_models import Vendor, VendorNew

@app.route("/keywordText",methods=['POST', 'GET'])
def keyword():
    inbound_message = request.args.get('text')
    return extractKeywordsText(inbound_message)

@app.route("/getKeywords")
def getKeywords():
    domain = request.args.get('url')
    keyword_info = getDomainKeywords(domain)
    return json.dumps(keyword_info)

@app.route("/scrape", methods=['POST', 'GET'])
def scrape():
    """Respond to incoming calls with a simple text message."""
    print request.form
    url = request.args.get('url')
    print "Scraping : " + url
    scraper =  Scraper()
    text = scraper.scrape(url)
    return ' '.join(text.split())

@app.route("/getLogo")
def getLogo():
    vendors = VendorNew.objects.all()
    app.logger.info('Preparing to loop through vendors')
    for v in vendors:
        if type(v.name) == float:
            name = str(v.name)
        else:
            name = v.name.encode('utf-8')
        domain = v.domain
        app.logger.info('     ##### Processing: %s #####', name)
        if v.logo:
            app.logger.info('%s already has a logo', name)
            continue
        else:
            scraper =  Scraper()
            logo = scraper.getLogo(name, domain)
            if (logo != "Not 200") and (logo != "Failed") and (logo != "No results match search"):
                app.logger.info('Logo found: %s', logo)
                v.logo = logo
                v.name = name
                v.save()
            else:
                app.logger.info('Error: %s', logo)
                v.logo = logo
                v.name = name
                v.save()
                continue
    return "Finished!"

@app.route("/getTwitterImages")
def getTwitterImage():
    vendors = Vendor.objects.all()
    app.logger.info('Preparing to loop throgh vendors')
    for v in vendors:
        if 'twitterImg' in v.__dict__:
            app.logger.info('Twitter image already present')
        else:
            vendor_name = v.name
            app.logger.info('getting twitter image for: %s', vendor_name)
            scraper =  Scraper()
            twitter_img = scraper.extractTwitterImage(vendor_name)
            app.logger.info('Twitter image found on the following url: %s', twitter_img)
            v.twitterImg = str(twitter_img)
            v.save()
    return "Process complete!"

@app.route("/summarize", methods=['POST', 'GET'])
def summ():
    """Respond to incoming calls with a simple text message."""
    print request.form
    inbound_message = request.args.get('text')
    print "recieved : " + inbound_message
    return summarizeText(inbound_message)

@app.route("/logo", methods=['POST', 'GET'])
def logoExtractor():
    inbound_message = request.args.get('url')
    scraper =  Scraper()
    return scraper.extractLogo(inbound_message)


def perform_search(keywords):
    client = algoliasearch.Client(app.config['ALGOLIA_APP_ID'], app.config['ALGOLIA_SEARCH_SECRET'])
    index = client.init_index('vendors')
    response = index.search(keywords)
    results = response['hits']

    return results


def perform_match(keywords,antiKeywords=None):
    print "Peforming match"
    import sys
    from ml.latent_representation import KeywordEncoder
    kwe = KeywordEncoder(latent_size=20)
    kwe.load('models/keywords')
    vendor_list = []
    score_list = []
    # anti_score_list = []
    # anti_vendor_list = []

    client = MongoClient(app.config['DB_HOST'])
    db = client[app.config['DB_NAME']]
    vendors = db.vendor.find({'$and': [{'domain.keywords': {'$exists': True}}, {'clearbit': {'$exists': True}}]})

    total_time = 0

    for vendor in vendors:
        vendor_keywords = list(vendor['domain']['keywords'].keys())
        import time
        start = time.time()
        score = kwe.get_distance(vendor_keywords, keywords)
        if antiKeywords and len(antiKeywords) > 0:
            antiscore = kwe.get_distance(vendor_keywords, antiKeywords)
            import numpy as np
            if antiscore<1e-8:
                antiscore=1e-8

            score = score+1.0/antiscore
            # anti_score_list.append(antiscore)
            # anti_vendor_list.append(vendor)
        stop = time.time()
        total_time += stop - start
        vendor_list.append(vendor)
        score_list.append(score)

    for i in xrange(len(vendor_list) - 1):
        for j in xrange(i + 1, len(vendor_list)):
            if score_list[j] < score_list[i]:
                aux = score_list[j]
                score_list[j] = score_list[i]
                score_list[i] = aux
                aux = vendor_list[i]
                vendor_list[i] = vendor_list[j]
                vendor_list[j] = aux

    # if antiKeywords:
    #     for i in xrange(len(vendor_list) - 1):
    #         for j in xrange(i + 1, len(vendor_list)):
    #             if anti_score_list[j] < anti_score_list[i]:
    #                 aux = anti_score_list[j]
    #                 anti_score_list[j] = anti_score_list[i]
    #                 anti_score_list[i] = aux
    #                 aux = anti_vendor_list[i]
    #                 anti_vendor_list[i] = anti_vendor_list[j]
    #                 anti_vendor_list[j] = aux
    result = []
    # if antiKeywords:
    #     anti_vendor_names = [anti_vendor_list[i].name for i in xrange(min(len(anti_vendor_list), 100))]
    # print anti_vendor_names
    for i in xrange(min(len(vendor_list), 100)):
        result.append({})
        result[i] = {}
        result[i]['vendor'] = vendor_list[i]
        # # if antiKeywords:
        # #     if vendor_list[i].name in anti_vendor_names:
        # #         continue
        # result[i]['vendor']['company_name'] = vendor_list[i]['company_name']
        # result[i]['vendor']['company_description'] = vendor_list[i]['clearbit']['description']
        # result[i]['vendor']['domain'] = vendor_list[i]['domain']['domain']
        # # result[i]['vendor']['email'] = vendor_list[i].email
        # result[i]['vendor']['keywords'] = vendor_list[i]['domain']['keywords']
        # result[i]['vendor']['logo'] = vendor_list[i]['clearbit']['logo']
        result[i]['score'] = 1 - score_list[i]
    
    result_ordered = sorted(result, key=lambda x: x['score'], reverse=True)
    # Comment out print to avoid console issues in Heroku
    # print json.dumps(result_ordered, sort_keys=True, indent=4, separators=(',', ': '))
    return result_ordered

@app.route("/match", methods=['POST', 'GET'])
def match():
    keywords=request.args.get('keywords').split(' ')
    result=perform_match(keywords)
    import json
    return json.dumps(result)
