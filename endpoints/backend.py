from server import app
import os
import sys
from bson.objectid import ObjectId
sys.path.insert(0,'../')
from flask import Flask, render_template, request, redirect, session, url_for
import json
from flask_cors import CORS
import datetime
from pymongo import MongoClient
from urlparse import urlparse
from utilities.get_domain_info import getDomainInfo
from utilities.clean_domain import cleanDomain
from utilities.get_clearbit_data import getClearbitData
from authlib.flask.client import OAuth
from functools import wraps
from six.moves.urllib.parse import urlencode

oauth = OAuth(app)

auth0 = oauth.register(
    'auth0',
    client_id = app.config['AUTH_CLIENT_ID'],
    client_secret = app.config['AUTH_CLIENT_SECRET'],
    api_base_url = app.config['AUTH_API_BASE_URL'],
    access_token_url = app.config['AUTH_ACCESS_TOKEN_URL'],
    authorize_url = app.config['AUTH_AUTHORIZE_URL'],
    client_kwargs = {
        'scope': 'openid profile',
    },
)

categories = [
    "Mobile Marketing",
    "Display & Programmatic Advertising",
    "Search & Social Advertising",
    "Native/Content Advertising",
    "Video Advertising",
    "Print",
    "Public Relations",
    "Mobile Apps",
    "Video Marketing",
    "Interactive Content",
    "Email Marketing",
    "Content Marketing",
    "Optimization, Personalization & Testing",
    "Digital Asset Management (DAM) & Marketing Resource Management (MRM)",
    "Search Engine Optimization (SEO)",
    "Marketing Automation & Campaign/Lead Management",
    "Content Management System (CMS) & Web Experience Management",
    "Call Analytics & Management",
    "Account-based Marketing (ABM)",
    "Events, Meetings & Webinars",
    "Social Media Marketing & Monitoring",
    "Advocacy, Loyalty & Referrals",
    "Influencers",
    "Feedback & Chat",
    "Community & Reviews",
    "Experience, Service & Success",
    "Customer Relationship Management (CRM)",
    "Retail & Proximity Marketing",
    "Channel, Partners & Local Marketing",
    "Sales Automation, Enablement & Intelligence",
    "Affiliate Marketing & Management",
    "Ecommerce Marketing",
    "Ecommerce Platforms & Carts",
    "Audience/Market Data & Data Enhancement",
    "Marketing Analytics, Performance & Attribution",
    "Mobile & Web Analytics",
    "Dashboard & Data Visualization",
    "Business/Customer Intelligence & Data Science",
    "iPaaS, Cloud/Data Integration & Tag Management",
    "Data Management Platform (DMP)",
    "Predictive Analytics",
    "Customer Data Platforms",
    "Talent Management",
    "Product Management",
    "Budgeting & Finance",
    "Collaboration",
    "Projects & Workflow",
    "Agile & Lean Management",
    "Vendor Analysis"
]

companySizes = [
    "Self employed",
    "1-10 employees",
    "11-50 employees",
    "51-200 employees",
    "201-500 employees",
    "501-1000 employees",
    "1001-5000 employees",
    "5001-10,000 employees",
    "10,001+ employees"
]

client = MongoClient(app.config['DB_HOST'])
db = client[app.config['DB_NAME']]

# Index

def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    if 'profile' not in session:
      # Redirect to Login page here
      return redirect(url_for('login'), 302)
    return f(*args, **kwargs)

  return decorated

@app.route("/")
def index():
    return render_template('index.html')

# Registration

@app.route("/register/brand")
def brandRegistration():
    return render_template('register-brand.html')

@app.route('/registerBrand', methods=['POST'])
def registerBrand():

    brand = {}

    # Get a cleaned version of the domain provided
    try:
        app.logger.info('Attempting to get a clean version of the domain provided')
        company_domain = cleanDomain(request.form['company_domain'])
    except Exception as e:
        app.logger.error('Error getting clean version of domain provided.')
        print e
        raise

    # Assign attributes to vendor object
    try:
        app.logger.info('Attempting to assign base attributes')
        brand['company_name'] = request.form['company_name']
        brand['company_domain'] = company_domain
        brand['name'] = request.form['name']
        brand['email'] = request.form['email']
        brand['password'] = request.form['password']
    except Exception as e:
        app.logger.error('Error assigning base attribites.')
        print e
        raise

    # Get domain info for the domain provided
    try:
        app.logger.info('Attempting to get info for domain')
        domain_info = getDomainInfo(company_domain)
        brand['domain'] = domain_info
    except Exception as e:
        app.logger.error('Error getting info for domain.')
        print e
        raise

    # Get clearbit data for domain
    try:
        app.logger.info('Attempting to get clearbit data for domain')
        clearbit_data = getClearbitData(company_domain)
        brand['clearbit'] = clearbit_data
    except Exception as e:
        app.logger.error('Error getting clearbit data for domain.')
        print e
        raise

    # Check if domain differs from clearbit domain
    try:
        if brand['domain'] != None and brand['clearbit'] != None:
            if brand['domain']['domain'] != None and brand['clearbit']['domain'] != None:
                if brand['domain']['domain'] == brand['clearbit']['domain']:
                    brand['domain']['differs_from_clearbit'] = False
                else:
                    brand['domain']['differs_from_clearbit'] = True
    except KeyError:
        pass

        

    # Add vendor object to collection
    try:
        app.logger.info('Attempting to insert into db collection')
        brand_id = db.brand.insert(brand)
    except Exception as e:
        app.logger.error('Error inserting into db collection.')
        print e
        raise
    
    # Set session properties
    try:
        session['user'] = str(brand_id)
        session['type'] = 'brand'
    except Exception as e:
        app.logger.error('Error assigning session properties.')
        print e
        raise
    
    return redirect(url_for('discover'), 302)

@app.route("/register/vendor")
def vendorRegistration():
    return render_template('register-vendor.html')

@app.route('/registerVendor', methods=['POST'])
def registerVendor():

    vendor = {}

    # Get a cleaned version of the domain provided
    try:
        app.logger.info('Attempting to get a clean version of the domain provided')
        company_domain = cleanDomain(request.form['company_domain'])
    except Exception as e:
        app.logger.error('Error getting clean version of domain provided.')
        print e
        raise

    # Assign attributes to vendor object
    try:
        app.logger.info('Attempting to assign base attributes')
        vendor['company_name'] = request.form['company_name']
        vendor['company_domain'] = company_domain
        vendor['name'] = request.form['name']
        vendor['email'] = request.form['email']
        vendor['password'] = request.form['password']
    except Exception as e:
        app.logger.error('Error assigning base attribites.')
        print e
        raise

    # Get domain info for the domain provided
    try:
        app.logger.info('Attempting to get info for domain')
        domain_info = getDomainInfo(company_domain)
        vendor['domain'] = domain_info
    except Exception as e:
        app.logger.error('Error getting info for domain.')
        print e
        raise

    # Get clearbit data for domain
    try:
        app.logger.info('Attempting to get clearbit data for domain')
        clearbit_data = getClearbitData(company_domain)
        vendor['clearbit'] = clearbit_data
    except Exception as e:
        app.logger.error('Error getting clearbit data for domain.')
        print e
        raise

    # Check if domain differs from clearbit domain
    try:
        if vendor['domain'] != None and vendor['clearbit'] != None:
            if vendor['domain']['domain'] != None and vendor['clearbit']['domain'] != None:
                if vendor['domain']['domain'] == vendor['clearbit']['domain']:
                    vendor['domain']['differs_from_clearbit'] = False
                else:
                    vendor['domain']['differs_from_clearbit'] = True
    except KeyError:
        pass

    # Add vendor object to collection
    try:
        app.logger.info('Attempting to insert into db collection')
        vendor_id = db.vendor.insert(vendor)
    except Exception as e:
        app.logger.error('Error inserting into db collection.')
        print e
        raise
    
    # Set session properties
    try:
        session['user'] = str(vendor_id)
        session['type'] = 'vendor'
    except Exception as e:
        app.logger.error('Error assigning session properties.')
        print e
        raise

    return redirect(url_for('dashboard'), 302)

# Login & authentication

# @app.route("/login", methods=['POST', 'GET'])
# def login():
#     if request.method == 'POST':
#         error = None
#         bAccounts = db.brand.find({'$and': [{'email': request.form['email']}, {'password': request.form['password']}]})
#         vAccounts = db.vendor.find({'$and': [{'email': request.form['email']}, {'password': request.form['password']}]})
#         if bAccounts.count() > 0:
#             print "Found", bAccounts.count(), "brand accounts. Logging user into [0]"
#             session['user'] = str(bAccounts[0]['_id'])
#             session['type'] = 'brand'
#             return redirect(url_for('discover'), code=302)
#         elif vAccounts.count() > 0:
#             print "Found", vAccounts.count(), "vendor accounts. Logging user into [0]"
#             session['user'] = str(vAccounts[0]['_id'])
#             session['type'] = 'vendor'
#             return redirect(url_for('dashboard'), code=302)
#         else:
#             error = "We couldn't find an account for the details provided."
#             return render_template('login.html', error=error)
#     else:
#         return render_template('login.html')


@app.route('/login')
def login():
    redirect_uri = app.config['BASE_URL'] + "/login/success"
    return auth0.authorize_redirect(redirect_uri=redirect_uri, audience=app.config['AUTH_AUDIENCE_URL'])

@app.route('/login/success')
def login_success():
    # Handles response from token endpoint
    auth0.authorize_access_token()
    resp = auth0.get('userinfo')
    userinfo = resp.json()

    # [JH] Commented out as want to revert to Auth0 only handling authentication rather than authorization
    # tenants = app.config['BASE_URL'] + '/claims/groups'
    # roles = app.config['BASE_URL'] + '/claims/roles'

    # Store the user information in flask session.
    session['jwt_payload'] = userinfo
    session['profile'] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],
        'picture': userinfo['picture'],
        # [JH] Commented out as want to revert to Auth0 only handling authentication rather than authorization
        # 'tenants': userinfo[tenants],
        # 'roles': userinfo[roles]
    }

    print json.dumps(userinfo, sort_keys=True, indent=4, separators=(',', ': '))
    print session
    return redirect(url_for('dashboard'), code=302)

# @app.route('/logout')
# def logout():
#     session['user'] = None
#     session['type'] = None
#     return redirect(url_for('index'), code=302)

@app.route('/logout')
def logout():
    redirect_uri = app.config['BASE_URL']
    # Clear session stored data
    session.clear()
    # Redirect user to logout endpoint
    params = {'returnTo': redirect_uri, 'client_id': auth0.client_id}
    return redirect(auth0.api_base_url + '/v2/logout?' + urlencode(params))

# Dashboard

# @app.route("/dashboard")
# def dashboard():
#     accountType=session['type']
#     if accountType == 'brand':
#         return render_template('brand/dashboard.html')
#     if accountType == 'vendor':
#         return render_template('vendor/dashboard.html')

@app.route("/dashboard")
@requires_auth
def dashboard():
    return render_template('brand/dashboard.html')

# Brand

@app.route("/discover")
@requires_auth
def discover():
    # from ml_backend import perform_match
    from ml_backend import perform_search
    if 'query' in request.args:
        query = request.args.get('query')
        keywords = query.split(' ')
        results=perform_search(keywords)
        # return json.dumps(results)
        return render_template('brand/discover.html', results=results, query=query)
    else:
        return render_template('brand/discover.html')

@app.route("/ask")
@requires_auth
def ask():
    brand = db.brand.find_one({'_id': ObjectId(session['user'])})
    briefs = list(db.brief.find({'brandAccountId': str(session['user'])}))
    return render_template('brand/ask.html', briefs=briefs)

@app.route("/ask/create")
@requires_auth
def createBrief():
    return render_template('brand/brief-create.html', categories=categories)

@app.route("/ask/<briefId>", methods=['POST', 'GET'])
@requires_auth
def brief(briefId):
    if request.method == 'POST':
        db.brief.update(
            {
                '_id': ObjectId(briefId)
            },
            {
                '$set': {
                    'description': request.form['description'],
                    'urgency': request.form['urgency'],
                    'budget': request.form['budget'],
                    'keywords': request.form['keywords'].split(','),
                    'categories': request.form.getlist('categories'),
                }
            }
        )
        return redirect("/ask/" + briefId , code=302)
    else:
        # from ml_backend import perform_match
        from ml_backend import perform_search
        brief = db.brief.find_one({'_id': ObjectId(briefId)})
        keywords = ' '.join(brief['keywords'])
        print keywords
        results=perform_search(keywords)
        print results
        return render_template('brand/brief.html', brief=brief, results=results)

@app.route("/ask/<briefId>/edit")
@requires_auth
def editBrief(briefId):
    brief = db.brief.find_one({'_id': ObjectId(briefId)})
    keywords = ','.join(map(str, brief['keywords']))
    return render_template('brand/brief-edit.html', brief=brief, keywords=keywords, categories=categories)

# Account

@app.route("/account")
@requires_auth
def account():
    accountType=session['type']
    if accountType == 'brand':
        brand = db.brand.find_one({'_id': ObjectId(session['user'])})
        keywords = brand.get('domain', {}).get('keywords')
        return render_template('brand/account.html', brand=brand, accountType=accountType, keywords=keywords)
    if accountType == 'vendor':
        vendor = db.vendor.find_one({'_id': ObjectId(session['user'])})
        solutions = db.solution.find({'vendorAccountId': session['user']})
        keywords = vendor.get('domain', {}).get('keywords')
        return render_template('vendor/account.html', vendor=vendor, solutions=solutions, accountType=accountType, keywords=keywords)

@app.route("/updateAccount", methods=['POST'])
@requires_auth
def updateAccount():

    if session['type'] == 'brand':
        account = db.brand.find_one({'_id': ObjectId(session['user'])})
    if session['type'] == 'vendor':
        account = db.vendor.find_one({'_id': ObjectId(session['user'])})

    try:
        if request.form['company_name'] != account.get('company_name'):
            db[session['type']].update(
                {'_id': account['_id']},
                {'$set': {'company_name': request.form['company_name']}}
            )
        if request.form['company_description'] != account.get('company_description'):
            db[session['type']].update(
                {'_id': account['_id']},
                {'$set': {'company_description': request.form['company_description']}}
            )
        if request.form['company_domain'] != account.get('company_domain'):
            # Get a cleaned version of the domain provided
            try:
                app.logger.info('Attempting to get a clean version of the domain provided')
                company_domain = cleanDomain(request.form['company_domain'])
                db[session['type']].update(
                    {'_id': account['_id']},
                    {'$set': {'company_domain': company_domain}}
                )
                # Get domain info for the domain provided
                try:
                    app.logger.info('Attempting to get info for domain')
                    domain_info = getDomainInfo(company_domain)
                    db[session['type']].update(
                        {'_id': account['_id']},
                        {'$set': {'domain': domain_info}}
                    )
                except Exception as e:
                    app.logger.error('Error getting info for domain.')
                    print e
                    raise
                # Get clearbit data for domain
                try:
                    app.logger.info('Attempting to get clearbit data for domain')
                    clearbit_data = getClearbitData(company_domain)
                    db[session['type']].update(
                        {'_id': account['_id']},
                        {'$set': {'clearbit': clearbit_data}}
                    )
                except Exception as e:
                    app.logger.error('Error getting clearbit data for domain.')
                    print e
                    raise
                # Check if domain differs from clearbit domain
                if account['domain']['domain'] and account['clearbit']['domain']:
                    if account.get('domain', {}).get('domain') == account.get('clearbit', {}).get('domain'):
                        db[session['type']].update(
                            {'_id': account['_id']},
                            {'$set': {'domain.differs_from_clearbit': False}}
                        )
                    else:
                        db[session['type']].update(
                            {'_id': account['_id']},
                            {'$set': {'domain.differs_from_clearbit': True}}
                        )
            except Exception as e:
                app.logger.error('Error getting clean version of domain provided.')
                print e
                raise
        if request.form['name'] != account.get('name'):
            db[session['type']].update(
                {'_id': account['_id']},
                {'$set': {'name': request.form['name']}}
            )
        if request.form['email'] != account.get('email'):
            db[session['type']].update(
                {'_id': account['_id']},
                {'$set': {'email': request.form['email']}}
            )
        if request.form['password'] != account.get('password'):
            db[session['type']].update(
                {'_id': account['_id']},
                {'$set': {'password': request.form['password']}}
            )
    except Exception as e:
        app.logger.error('Error assigning base attribites.')
        print e
        raise

    return redirect(url_for('account'), 302)

@app.route("/account/solutions/create")
@requires_auth
def createSolution():
    return render_template('vendor/solution-create.html', categories=categories)

@app.route('/addSolution', methods=['POST'])
@requires_auth
def addSolution():
    vendor = db.vendor.find_one({'_id': ObjectId(session['user'])})
    solution = {}
    solution['vendorAccountId'] = str(session['user'])
    solution['name'] = request.form['name']
    solution['description'] = request.form['description']
    solution['url'] = request.form['url']
    solution['keywords'] = request.form['keywords'].split(',')
    solution['categories'] = request.form.getlist('categories')

    solution_id = db.solution.insert(solution)

    return redirect(url_for('account'), 302)

@app.route('/addBrief', methods=['POST'])
@requires_auth
def addBrief():
    brand = db.brand.find_one({'_id': ObjectId(session['user'])})
    brief = {}
    brief['brandAccountId'] = str(session['user'])
    brief['description'] = request.form['description']
    brief['urgency'] = request.form['urgency']
    brief['budget'] = request.form['budget']
    brief['keywords'] = request.form['keywords'].split(',')
    brief['categories'] = request.form.getlist('categories')

    brief_id = db.brief.insert(brief)

    return redirect(url_for('brief', briefId=brief_id), 302)

@app.route('/addMatch', methods=['POST', 'GET'])
@requires_auth
def addMatch():
    match = Match(
        brandId=request.args.get('brandId'),
        matchType=request.args.get('description'),
        briefId = request.args.get('briefId'),
        solutionId = request.args.get('solutionId'),
        score=request.args.get('score')
    )
    match.save()
    return json.dumps(request.json)
