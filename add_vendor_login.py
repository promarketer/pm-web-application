from pymongo import MongoClient
from multiprocessing.dummy import Pool as ThreadPool

# Set db connection
client = MongoClient('mongodb://127.0.0.1:27017')
# Set db
db = client['promarketer']
# Get vendor collection
vendors = db.vendor

def addVendorLogin(v):
	
	## Set vendor domain ##
	vendor_id = v['_id']

	## Set vendor domain ##
	domain = v['company_domain']
	password = "pmsecret"

	email = "test@" + domain

	try:
		vendors.update(
			{
				'_id': vendor_id
			},
			{
				'$set': {
					'email': email,
					'password': password,
				}
			}
		)
	except:
		print "Unable to update vendor"

if __name__ == '__main__':

	scope = list(vendors.find())

	pool = ThreadPool(1) 
	pool.map(addVendorLogin, scope)
	pool.close() 
	pool.join()