# Useful example tutorial https://realpython.com/flask-by-example-part-1-project-setup/

import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'i#Uy&L2^xE5DzCjb'
    SESSION_TYPE = 'filesystem'

class ProductionConfig(Config):
    BASE_URL = 'https://demo.promarketer.io'
    DB_NAME = 'heroku_1bq2h5jn'
    DB_HOST = 'mongodb://test:password123@ds217671.mlab.com:17671/heroku_1bq2h5jn'
    GTM_AUTH = "gv2i2EfIUyXR-b_HwV2HgA"
    GTM_PREVIEW = "env-9"
    # Auth0
    AUTH_CLIENT_ID = 'i8PvfMZEZg8luLhkgUW0WjIb7FokNkft'
    AUTH_CLIENT_SECRET = 'OpuemHKX7rVEUBnYs6XQbFQeFGCiM5nLtV1FxGi7n7ONbtQ0iLGfrf-YoyPf4h1-'
    AUTH_API_BASE_URL = 'https://login.promarketer.io'
    AUTH_ACCESS_TOKEN_URL = 'https://login.promarketer.io/oauth/token'
    AUTH_AUTHORIZE_URL = 'https://login.promarketer.io/authorize'
    AUTH_AUDIENCE_URL = 'https://promarketer.eu.auth0.com/userinfo'
    # Algolia search
    ALGOLIA_APP_ID = 'YM2WV0YEIF'
    ALGOLIA_SEARCH_SECRET = 'beaa5b3f169b2bcc431b6f8499ba7381'

class StagingConfig(Config):
    BASE_URL = 'https://staging.promarketer.io'
    DB_NAME = 'heroku_m3dgn6d8'
    DB_HOST = 'mongodb://test:password123@ds139331.mlab.com:39331/heroku_m3dgn6d8'
    GTM_AUTH = "qa42pDU82dKqpfFY-3x1bg"
    GTM_PREVIEW = "env-8"
    # Auth0
    AUTH_CLIENT_ID = 'Zd5xY2BLXnysXULog8PMawYa0G1wYq66'
    AUTH_CLIENT_SECRET = 'JJEvsBVMbbESJU7r2HuHETfXDfEB5kzenOcgX2_YaQevAacK2klHwcrEfBIM_M79'
    AUTH_API_BASE_URL = 'https://promarketer-staging.eu.auth0.com'
    AUTH_ACCESS_TOKEN_URL = 'https://promarketer-staging.eu.auth0.com/oauth/token'
    AUTH_AUTHORIZE_URL = 'https://promarketer-staging.eu.auth0.com/authorize'
    AUTH_AUDIENCE_URL = 'https://promarketer-staging.eu.auth0.com/userinfo'
    # Algolia search
    ALGOLIA_APP_ID = 'QFIIHB5BDH'
    ALGOLIA_SEARCH_SECRET = '6a070e2ebddde2f4e36f097529ee9de5'

class TestConfig(Config):
    BASE_URL = 'https://test.promarketer.io'
    DB_NAME = 'heroku_ngndmn7s'
    DB_HOST = 'mongodb://test:password123@ds139331.mlab.com:39331/heroku_ngndmn7s'
    GTM_AUTH = "z_A6nJyBCV-t0yL4gimeEw"
    GTM_PREVIEW = "env-7"
    # Auth0
    AUTH_CLIENT_ID = '5DuTfij32mEJDiQlyW3fIse3IJ8m3Fz3'
    AUTH_CLIENT_SECRET = 'O1UFe-8IuJCFtYA-P8WA0F5uLMP6wHzP3GlgXmSh8XyYUp_piklAAlexFLrT63Dz'
    AUTH_API_BASE_URL = 'https://promarketer-test.eu.auth0.com'
    AUTH_ACCESS_TOKEN_URL = 'https://promarketer-test.eu.auth0.com/oauth/token'
    AUTH_AUTHORIZE_URL = 'https://promarketer-test.eu.auth0.com/authorize'
    AUTH_AUDIENCE_URL = 'https://promarketer-test.eu.auth0.com/userinfo'
    # Algolia search
    ALGOLIA_APP_ID = '318U9N3HNM'
    ALGOLIA_SEARCH_SECRET = 'fd96d7155194ee67b2ac3ba492ec79af'

class DevelopmentConfig(Config):
    BASE_URL = 'http://localhost:8000'
    DB_NAME = 'promarketer'
    DB_HOST = 'mongodb://127.0.0.1:27017'
    DB_PORT = 27017
    GTM_AUTH = "t27PcmBFMat0Z__VcCCKCw"
    GTM_PREVIEW = "env-5"
    # Auth0
    AUTH_CLIENT_ID = 'mDBSBgSoAG9yiBW5pwYkVix0xve8235M'
    AUTH_CLIENT_SECRET = 'mhSgn2EkJGOJecJjDU6ngTO0uKD2ip1V-ft0F6Qu0I3aqZnPzj-_JNN_H_lrsre-'
    AUTH_API_BASE_URL = 'https://promarketer-dev.eu.auth0.com'
    AUTH_ACCESS_TOKEN_URL = 'https://promarketer-dev.eu.auth0.com/oauth/token'
    AUTH_AUTHORIZE_URL = 'https://promarketer-dev.eu.auth0.com/authorize'
    AUTH_AUDIENCE_URL = 'https://promarketer-dev.eu.auth0.com/userinfo'
    # Algolia search
    ALGOLIA_APP_ID = 'U59LRUV977'
    ALGOLIA_SEARCH_SECRET = '31996554ab12c046b7beb1cb96f852b1'