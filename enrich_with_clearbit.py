import clearbit
import json
import sys
import time
import requests as rq
from pymongo import MongoClient
from bson import ObjectId
from multiprocessing.dummy import Pool as ThreadPool

## Set clearbit key ##
clearbit.key = 'sk_d53647ca6403dfa44b942e67e012abad'
## Set mongo client ##
client = MongoClient('mongodb://127.0.0.1:27017/')
## Get mongo DB ##
db = client['promarketer2']
## Get mongo collection ##
vendors = db.vendors_stage23

def enrichWithClearbit(v):

	## Get vendor domain ##
	vendor_domain = v['domain']['domain']
	vendor_id = v['_id']

	try:

		print "Searching clearbit for", vendor_domain

		## Look for a match for the domain on clearbit ##
		result = clearbit.Enrichment.find(domain=vendor_domain, stream=True)
		
		## Check if match was found
		if result != None:

			try:

				## Add data to record ##
				# print "Result found attempting to adding data to record."
				vendors.update(
					{
						'_id': vendor_id
					},
					{
						'$set': {
							'clearbit': result
						}
					}
				)

			except:

				print "Failed to add data to record."

		else:

			print "Received a result of None"

	except:

		print "Failed to get data from clearbit"

if __name__ == '__main__':

	scope = list(vendors.find({ '$and': [{"domain.domain": {'$exists': True}}, {"clearbit": {'$exists': False}}]}))
	# scope = list(vendors.find({ 'clearbit': { '$exists': False }}))
	# scope = list(vendors.find({ "$or": [ {'company_domain':'140proof.com'}, {'company_domain':'aarki.com'}, {'company_domain':'360dialog.com'} ] }))
	# scope = list(vendors.find({ 'domain.ok': False}))

	pool = ThreadPool(50) 
	pool.map(enrichWithClearbit, scope)
	pool.close() 
	pool.join()