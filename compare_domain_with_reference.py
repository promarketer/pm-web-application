from pymongo import MongoClient
from multiprocessing.dummy import Pool as ThreadPool

# Set db connection
client = MongoClient('mongodb://127.0.0.1:27017/')
# Set db
db = client['promarketer2']
# Get vendor collection
vendors = db.vendors_stage23

def compare_domain_with_clearbit(v):

	## Set vendor domain ##
	vendor_id = v['_id']
	reference = v['domain']['reference']
	domain = v['domain']['domain']
	
	print "Comparing:", domain

	if domain != reference:
		vendors.update(
			{
				'_id': vendor_id
			},
			{
				'$set': {
					'domain.differs_from_reference': True,
				}
			}
		)
	else:
		vendors.update(
			{
				'_id': vendor_id
			},
			{
				'$set': {
					'domain.differs_from_reference': False,
				}
			}
		)

if __name__ == '__main__':

	scope = list(vendors.find({ '$and': [{'domain.domain': { '$exists': True }}, {'domain.reference': { '$exists': True }}]}))

	pool = ThreadPool(1)
	pool.map(compare_domain_with_clearbit, scope)
	pool.close() 
	pool.join()