from pymongo import MongoClient

# Set db connection
client = MongoClient('mongodb://127.0.0.1:27017/')
# Set db
db = client['promarketer2']
# Get vendor collection
vendors = db.vendors_stage23

# Get scope of vendors
scope = list(vendors.find({'domain.keywords': {'$exists': True}}))
# scope = list(vendors.find({ "$or": [ {'company_domain':'140proof.com'}, {'company_domain':'aarki.com'}, {'company_domain':'360dialog.com'} ] }))

keywords = []

for index, v in enumerate(scope, start=1):

	vendor_id = v['_id']

	vendor_keywords = v['domain']['keywords']

	# print vendor_keywords

	vendor_keywords_list = list(vendor_keywords.keys())
	vendor_keywords_string = ' '.join(vendor_keywords_list).encode('utf-8').strip()

	# print vendor_keywords_string
	
	file = open("vendor_keywords_aug.txt","a")
	file.write(vendor_keywords_string + "\n")
	file.close()