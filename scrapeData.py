from endpoints.ml.Scraper import Scraper
import pandas as pd 
import json
import os
import requests as rq 
from gensim.summarization import summarize , keywords
import urllib

def extractByGensim(text):
	result = {}
	try:
		keyWords = keywords(text , scores = True )
	except:
		print "DEcoding Error"
		return {}
	for k in keyWords:
		result[k[0]] = k[1]
	return result

company = "/Users/sagarj/Work/data/merge_all_v1.csv"

data = pd.read_csv(company)

crawledData = dict()

batchSize = 100
scraper = Scraper()

scrapedkeys = [k.split('.')[0] for k in os.listdir("scrapedData/")]

sample = data[:10]
for col, row in data.iterrows():
	if type(row['Proposition']) == str and type(row['Product']) == str:
		scraped = dict()
		payload = {}
		payload['name'] = row['Name']
		payload['url'] = row['Website']
		payload['description'] = row['Proposition']
		payload['email'] = row['Name']+"@"+ row['Name']+".com"
		payload['password']= "secret123"
		
		product = row['Product']
		filtered = []
		webKeys = []
		
		if row['Name'] not in scrapedkeys:
			url = row['Website']
			print "Scraping: " + url
			scrapeText = None
			try:
				scrapeText = scraper.scrape(url)
				webKeys = extractByGensim(scrapeText)
			except:
				print "couldn't scrape"
				continue

			if scrapeText:
				try:
					scraped[row['Name']] = scrapeText
					with open("scrapedData/" + row['Name'].replace("/", "")  +".json", 'w') as outfile:
						json.dump(scraped, outfile)
				except:
					print "Could not save file"
					continue	

		else:
			print "already done!!"
			continue
		
		prop = product + row['Proposition']
		keyW = extractByGensim(prop)
		if len(webKeys) > 0:
			for k in webKeys:
				if webKeys[k] > 0.15:
					filtered.append(k.strip())
		for k in keyW:
			filtered.append(k.strip())
		if len(filtered) == 0:
			continue
		payload['keywords'] = ' '.join(filtered)
		# print payload
		try:
			url = "http://127.0.0.1:5005/addVendor?"+urllib.urlencode(payload)
			print url
			r = rq.get(url)
			print r.status_code
		except:
			print "Something went wrong"







