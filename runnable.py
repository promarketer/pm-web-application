from flask import Flask, request, redirect
from Scraper import *
from gensim.summarization import summarize
from rake_nltk import Rake,Metric

def summarizeText(text):
	try:
		summary = summarize(text)
	except:
		print "Something went wrong"
		summary = text
	return summary

def extractKeywordsText(text):
	r = Rake()
	keywords = {}

	try:
		r.extract_keywords_from_text(text)
		phrases = r.get_ranked_phrases()
		summary = ""

	except:
		print "Something went wrong"
		summary = ""
	return summary

app = Flask(__name__)

@app.route("/scrape", methods=['POST'])
def scrape():
    """Respond to incoming calls with a simple text message."""
    print request.form
    url = request.form.get('url')
    print "Scraping : " + url
    scraper =  Scraper()
    text = scraper.scrape(url)
    # print text
    return ' '.join(text.split())

@app.route("/summarize", methods=['POST'])
def summ():
    """Respond to incoming calls with a simple text message."""
    print request.form
    inbound_message = request.form.get('url')
    print "recieved : " + inbound_message
    return summarizeText(inbound_message)



if __name__ == "__main__":
    app.run(debug=False)

    # text = "Thomas A. Anderson is a man living two lives. By day he is an average computer programmer and by night a hacker known as Neo. Neo has always questioned his reality, but the truth is far beyond his imagination. Neo finds himself targeted by the police when he is contacted by Morpheus, a legendary computer hacker branded a terrorist by the government. Morpheus awakens Neo to the real world, a ravaged wasteland where most of humanity have been captured by a race of machines that live off of the humans' body heat and electrochemical energy and who imprison their minds within an artificial reality known as the Matrix. As a rebel against the machines, Neo must return to the Matrix and confront the agents: super-powerful computer programs devoted to snuffing out Neo and the entire human rebellion."
    # print summarizeText(text)