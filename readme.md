## Overview of the platform
The platform consists of three components. The frontend, backend and the matching logic. To the best of our knowledge these three prime components are functional and can be hosted
## Tech
The whole stack is based on Python flask, with other libraries for the matching. The frontend is bootstrap, angular and some vanilla HTML.

## Frontend.
The frontend is based on Flask Jinja templating and bootstrap override and custom theming.

## Backend.
The platform backend is supported by MongoDB database and flask endpoints. Each endpoint is stateless as of now and the actions are limited to the purvey of the call.

## Matching logic.
The matching of vendors to brands is based on matching of Keywords extracted from the page, the description and the brief in a 20 dimensional latent space.
We construct the representations of these keywords in this space using an auto encoder written in [Dynet](https://github.com/clab/dynet)

To arrive at the keywords we use a pretrained model in [gensim](https://radimrehurek.com/gensim/) which allows us to seamlessly create language graph based on any given text and generate keywords based on highest page ranks in this graph. Once the keywords are generated across a dataset of around 2500 vendors, the dynet auto-encoder is trained to convert any list of keywords into a 20 dimensional vector. This vector is then used to make matched between the descriptions of briefings and the descriptions of vendors

## Installation.
The whole repository is a single package that contains both the backend and frontend parts along with a pre-trained model and the matching logic.
To launch the app, you can either use sandbox environment like virtualenv and the install all the dependencies using pip. `pip install -r requirements.txt`

Once the requirements are setup, you will need to point the data_models.py to the right address and port for the mongo db. As of today this setup is completely hardcoded, but can be made configurable later. The connect_to_mongo method of storage.data_models package is where the configuration is done.
After that, all you need to do is run `python server.py` and the application should start running on local host port 5005



useful instruction for backend running:

dependencies:

sudo pip install flask
sudo pip install flask_cors
sudo pip install mongoengine==0.10.7

run command:

sudo python server.py


INFO:
The server will run on localhost port 5005 by default

Known issues:

When installing requirements.txt for the first time it may be necessary to
remove rake-nltk==1.0.3 first, install the other packages then add
rake-nltk==1.0.3 back in and install requirements.txt again.


# Description of the Machine Learning Algorithms

This section of the documentation handles the data acquisition and processing steps involved in providing the intelligent matching system between brands and vendors.
The algorithm is centered on latent knowledge representations that are computed on the fly using domain-specific keywords. The keywords are computed automatically during model bootstrapping and they can be updated at any time by retraining the models.
The standard training pipeline is:
1.	Use NLP to extract keywords from product/vendor/brand/solution descriptions and create a training file (details will follow)
2.	Filter our uncommon keywords (occurrence < threshold) and cleanup the dataset
3.	Train an auto-encoder in an unsupervised manner to learn latent knowledge representations which capture dependencies at many levels such as: domain, categories, keywords etc.

Once the models are trained the auto-encoder is used to project any type of input into a latent space, which is later used to measure the similarity between solutions/vendors/brands/briefs (referred to as objects) in two ways:
1.	Best fit between two objects is computed by measuring the Euclidian distance in the latent-space between the two projections computed using the same auto-encoder
2.	Complex queries such as “best match between” and “exclude results that refer to”, are computed using a customized scoring function. Say you want to find objects that are close to A and distant to B, we compute the score of object C as the sum between `match(C, A) + 1.0/match(C, B)`. To avoid numeric underflow we force `match (C, B)` to be at least `1e-8`.
## Breakdown of operations
In order to extract keywords from textual summaries, one can use any NLP software that can extract a the list of terms from plain text, or can achieve this using the integrated endpoint `/keywordText` and passing on the `GET` parameter `text`.
The keyword file should have a single line for every example, with WHITESPACE separated keywords.
Text filtering is internally achieved when training the auto-encoder. In order to simply train a model you can use the CLI interface by typing (in the root folder of the solution):
```bash
python  endpoints/ml/latent_representation.py –train <keyword file> <model store base>
```
The platform will automatically load models that are stored as ‘models/keywords’ (base filename)
To use the default keywords and create a new model just use
```bash
python  endpoints/ml/latent_representation.py –train corpus/Keywords_MTM.txt models/keywords
```
Vizualizations are stored in the <model output base>.png.
For the case above, after model convergence (the training will automatically stop - but you can just end it at any time), the output might look look like:

![alt text](../models/keywords.png)


# Scraping , Summarisation and keywords logic

## Scraper: 
One of the functions of the automated tagging and information collection is the scraping function of the platform. We have written a custom scraper, that tries to get all the visible text areas from the web pages or any URLs that you throw at it. 
This scraper is integral during sign-up of brands and vendors, as at this point we try to acquire as many tags as possible from their web page. The scraper is also important to populate a large dataset of vendors to our database. A utility script called `ScrapeData.py` is especially handy for this. The scripts reads from a .csv file which follows the column format of the .xlsx file which was provided as part of the hack and then goes though each row, to 
* Extract the keywords from the description of the vendor,
* Extract keywords from the text scraped from the vendor's url
* Serialize everything into a payload that can be written to the mongo and then make the request to write it 
The scraper class can be found at `endpoints/ml/Scraper.py`. 

## Summarisation and Keywords
The summarisation and keywords extraction is done using a pre-trained learned model that can estimate the importance of words in a text based on its word association graph. The framework used for this is called [gensim](https://radimrehurek.com/gensim/). The code that does the extraction can be found at `endpoints/ml/ml.py`




