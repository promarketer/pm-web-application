from pymongo import MongoClient
from slugify import slugify

# Set db connection
client = MongoClient('mongodb://127.0.0.1:27017/')
# Set db
db = client['promarketer2']
# Get vendor collection
vendors = db.vendors_stage23

# Get scope of vendors
scope = list(vendors.find())

for index, v in enumerate(scope, start=1):

	print index, "- Running merge for", v['company_domain']

	vendor_id = v['_id']
	vendor_domain = v['company_domain']
	vendor_name = v['company_name']

	# Find duplicates by name and domian
	dups = vendors.find({'company_name': vendor_name, 'company_domain': vendor_domain})

	# Find duplicates by name only
	# dups = vendors.find({'company_name': vendor_name})

	# Find duplicates by domain only
	# dups = vendors.find({'company_domain': vendor_domain)

	if dups.count() > 1:

		print "Dups found. Building categories."

		parent_categories = []
		child_categories = []

		for d in dups:

			for c in d['parent_categories']:

				if c not in parent_categories:

					parent_categories.append(c)

			for c in d['child_categories']:

				if c not in child_categories:

					child_categories.append(c)

		print "Settings values."

		vendors.update(
			{'_id': vendor_id},
			{'$set':
				{
					'parent_categories': parent_categories,
					'child_categories': child_categories
				}
			}
		)